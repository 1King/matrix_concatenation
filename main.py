import itertools

def generate(lst):
    result = []
    for combination in itertools.product(*lst):
        result.append(" ".join(str(x) for x in combination))
    return result

if __name__ == "__main__":
    print(
        generate(
            [
                [1, 2, 3],
                [4, 5, 6],
            ]
        )
    )